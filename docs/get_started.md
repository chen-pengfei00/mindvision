# 环境依赖

- numpy 1.17+
- opencv-python 4.1+
- pytest 4.3+
- [mindspore](https://www.mindspore.cn/install) 1.2+

# 安装

## 环境准备

- 创建一个conda虚拟环境并且激活。

```shell
conda create -n mindvision python=3.7.5 -y
conda activate mindvision
```

- 安装MindSpore

```shell
pip install https://ms-release.obs.cn-north-4.myhuaweicloud.com/1.3.0/MindSpore/ascend/aarch64/mindspore_ascend-1.3.0-cp37-cp37m-linux_aarch64.whl --trusted-host ms-release.obs.cn-north-4.myhuaweicloud.com -i https://pypi.tuna.tsinghua.edu.cn/simple
```

## 安装MindVision

- 使用git克隆MindVsion仓库。

```shell
git clone https://gitee.com/mindspore/mindvision.git
cd mindvision
```

- 安装

```python
python setup.py install
```

## 验证

为了验证MindVision和所需的环境是否正确安装，我们可以运行示例代码来初始化一个分类器然后推理一张图片：

```shell
update
```

如果您成功安装，以上代码应该会成功运行。
