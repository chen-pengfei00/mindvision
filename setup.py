#!/usr/bin/env python
"""
setup
"""

from setuptools import find_packages
from setuptools import setup

version = '0.0.1'

setup(
    name="mindvision",
    version=version,
    author="MindVision Core Team",
    url="https://gitee.com/mindspore/mindvision/tree/master/",
    packages=find_packages(exclude=("example")),
)
