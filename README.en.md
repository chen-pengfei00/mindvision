# MindVision

[Docs] | [中文版]

[Docs]: https://github.com/RUCAIBox/RecBole
[中文版]: README.md

## Introduction

MindVision is an open source computer vision research toolbox based on MindSpore.

The master branch works with **MindSpore 1.2**.

## Installation

Please refer to [get_started.md]() for installation.

## License

This project is released under the [Apache 2.0 license](LICENSE).

## Feedbacks and Contact

The dynamic version is still under development, if you find any issue or have an idea on new features, please don't hesitate to contact us via [Gitee Issues](https://gitee.com/mindspore/mindvision/issues).

## Projects in MindVision

- [Classification](mindvision/classification/README.en.md): MindVision image classification toolbox and benchmark.
- [Detection](): MindVision detection toolbox and benchmark.

## Acknowledgement

MindSpore is an open source project that welcome any contribution and feedback.
We wish that the toolbox and benchmark could serve the growing research
community by providing a flexible as well as standardized toolkit to reimplement existing methods
and develop their own new computer vision methods.

## Contributing

We appreciate all contributions to improve MindVision. Please refer to [CONTRIBUTING.md](CONTRIBUTING.md) for the contributing guideline.

## Citation

If you find this project useful in your research, please consider citing:

```latex
@misc{mindvsion2021,
    title={{MindVision}:MindSpore Vision Toolbox and Benchmark},
    author={MindVision Contributors},
    howpublished = {\url{https://gitee.com/mindspore/mindvision}},
    year={2021}
}
```