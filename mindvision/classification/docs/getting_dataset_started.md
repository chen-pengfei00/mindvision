# 数据处理快速入门

此教程主要介绍MindVision中数据处理的接口，方便用户可以快速地对常用数据集进行数据加载和数据增强等操作。

## 数据的准备与处理

### 下载并解压数据集

我们示例中用到的MNIST数据集是由10类28∗28的灰度图片组成，训练数据集包含60000张图片，测试数据集包含10000张图片。

- 方式一：

你可以从MNIST数据集下载页面下载，并按下方目录结构放置。MNIST数据集下载页面：http://yann.lecun.com/exdb/mnist/ 页面提供4个数据集下载链接，其中前2个文件是训练数据需要，后2个文件是测试结果需要。

- 方式二：

在Jupyter Notebook中执行如下命令下载MNIST数据集。

```python
!mkdir -p ./datasets/MNIST_Data/train ./datasets/MNIST_Data/test
!wget -NP ./datasets/MNIST_Data/train https://mindspore-website.obs.myhuaweicloud.com/notebook/datasets/mnist/train-labels-idx1-ubyte
!wget -NP ./datasets/MNIST_Data/train https://mindspore-website.obs.myhuaweicloud.com/notebook/datasets/mnist/train-images-idx3-ubyte
!wget -NP ./datasets/MNIST_Data/test https://mindspore-website.obs.myhuaweicloud.com/notebook/datasets/mnist/t10k-labels-idx1-ubyte
!wget -NP ./datasets/MNIST_Data/test https://mindspore-website.obs.myhuaweicloud.com/notebook/datasets/mnist/t10k-images-idx3-ubyte
!tree ./datasets/MNIST_Data
```

## 接口使用方式

### 导入模块

```python
from mindvison.classification.dataset import create_mnist_dataset
```

### 参数介绍

`create_mnist_dataset`函数对MNIST数据集进行了数据处理pipeline操作，其中包括数据加载、数据增强、数据混洗和数据分批等操作，参数如下：

- `data_path`：MNIST训练数据集或者测试数据集的路径。
- `batch_size`：分批的大小，默认值是32。
- `resize_height`：图像调整大小后的高度，默认值是32像素。
- `resize_width`：图像调整大小后的宽度，默认值是32像素。
- `num_paraller_workers`：多线程并行数，默认值是1。

### 数据处理

- 使用`create_mnist_dataset`函数对MNIST训练数据集进行数据处理pipeline操作。

```python
data_path = './datasets/MNIST_Data/train'
batch_size = 4

mnist_ds = create_mnist_dataset(data_path=data_path, batch_size=batch_size)
```

- 取出一批batch数据，查看其数据张量及labe。

    ```python
    data = next(mnist_ds.create_dict_iterator(output_numpy=True))
    images = data["image"]
    labels = data["label"]
    print('Tensor of image:', images.shape)
    print('Labels:', labels)
    ```

    结果：

    ```text
    Tensor of image: (4, 1, 32, 32)
    labels: [8 8 7 0]
    ```

- 可视化处理后的数据。

    ```python
    import matplotlib.pyplot as plt

    count = 1

    for i in images:
        plt.subplot(1, 4, count)
        plt.imshow(np.squeeze(i))
        plt.title('num:%s'%labels[count-1])
        plt.xticks([])
        count += 1
        plt.axis("off")

    plt.show()
    ```

    结果：

    ![png](./images/mnist.png)