# ShuffleNet V2: Practical Guidelines for Efficient CNN Architecture Design

## 简介

ShuffleNetV2是基于ShuffleNetV1改进的轻量级网络，设计目的是能让网络又准确又快地运行在移动端设备。模型的运行速度不仅仅需要与模型的计算复杂度（即FLOPS）有关，还与模型的运行内存消耗和计算平台的特性有关。考虑到以上因素，ShuffleNetV2提出了一个新的运算：channel split, 采用深度可分割卷积（depthwise separable convolutions），减少了组卷积（group convolution）模块，减少网络碎片化以提高并行度，并且减少了元素级操作（element-wise operators），使ShuffleNetV2的速度和精度都达到一个较高的水平。

[ShuffleNet V2: Practical Guidelines for Efficient CNN Architecture Design](https://arxiv.org/abs/1807.11164.pdf)

```latex
@inproceedings{ma2018shufflenet,
  title={Shufflenet v2: Practical guidelines for efficient cnn architecture design},
  author={Ma, Ningning and Zhang, Xiangyu and Zheng, Hai-Tao and Sun, Jian},
  booktitle={Proceedings of the European conference on computer vision (ECCV)},
  pages={116--131},
  year={2018}
}
```

## 模型结构

ShuffleNetV2总体网络架构如下：

[ShuffleNet V2: Practical Guidelines for Efficient CNN Architecture Design](https://arxiv.org/abs/1807.11164.pdf)

## 训练参数

| Parameters                 | ShuffleNetV2                                               |
| -------------------------- | ---------------------------------------------------------- |
| Model Version              | V2                                                         |
| Resource                   | NV GTX2080Ti; memory 11G; OS Ubuntu18.04                   |
| uploaded Date              | 07/23/2021                                                 |
| MindSpore Version          | 1.2.0                                                      |
| Training Parameters        | shufflenetv2_x1_0_imagenet2012.yaml                        |
| Optimizer                  | Momentum                                                   |
| Loss Function              | CrossEntropySmooth                                         |
| Backbone                   | ShuffleNetV2                                               |
| Neck                       | AvgPooling                                                 |
| Head                       | DenseHead                                                  |
| Batch size                 | 256                                                        |

## 验证参数

| Parameters                 | ShuffleNetV2                                               |
| -------------------------- | ---------------------------------------------------------- |
| Model Version              | V2                                                         |
| Resource                   | NV GTX2080Ti; memory 11G; OS Ubuntu18.04                   |
| uploaded Date              | 07/23/2021                                                 |
| MindSpore Version          | 1.2.0                                                      |
| Training Parameters        | shufflenetv2_x1_0_imagenet2012.yaml                        |
| Backbone                   | ShuffleNetV2                                               |
| Neck                       | AvgPooling                                                 |
| Head                       | DenseHead                                                  |
| Batch size                 | 256                                                        |

## 预训练模型

### 在ImageNet上训练后的预训练文件

|         Model         | Params(M) | Flops(G) | Top-1 (%) | Top-5 (%) | Config | Download |
|:---------------------:|:---------:|:--------:|:---------:|:---------:|:---------:|:--------:|
| ShuffleNet V2_x0.5          |        |     |  |  | [config]() | [model]() |
| ShuffleNet V2_x1.0           |        |     | 69.65 | 88.63 | [config](https://gitee.com/mindspore/mindvision/blob/classification_shufflenetv2/mindvision/classification/config/shufflenetv2/shufflenetv2_x1_0_imagenet2012.yaml) | [model]() |
| ShuffleNet V2_x1.5           |        |     |  |  | [config]() | [model]() |
| ShuffleNet V2_x2.0           |        |     |  |  | [config]() | [model]() |

## 样例

点击此链接 [link]() 以了解更多信息.
