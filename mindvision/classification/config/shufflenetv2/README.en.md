# ShuffleNet V2: Practical Guidelines for Efficient CNN Architecture Design

## Introduction

The ShufflenetV2 architecture is designed to speed up image classification computation. The speed of deep nerual network not only depend on computation complexity, i.e. FLOPS, but also depends on the other factors such as memory access cost and platform characterics. Taking these factors into account, this work proposes practical guidelines for efficient network design. Based on ShuffleNetV1, ShuffleNetV2 applies "channel split" to reduce the group convolution and element-wise operators.

[ShuffleNet V2: Practical Guidelines for Efficient CNN Architecture Design](https://arxiv.org/abs/1807.11164.pdf)

```latex
@inproceedings{ma2018shufflenet,
  title={Shufflenet v2: Practical guidelines for efficient cnn architecture design},
  author={Ma, Ningning and Zhang, Xiangyu and Zheng, Hai-Tao and Sun, Jian},
  booktitle={Proceedings of the European conference on computer vision (ECCV)},
  pages={116--131},
  year={2018}
}
```

## Model architecture

The overall network architecture of ShuffleNetV2 is show below:

[ShuffleNet V2: Practical Guidelines for Efficient CNN Architecture Design](https://arxiv.org/abs/1807.11164.pdf)

## Training setting

| Parameters                 | ShuffleNetV2                                               |
| -------------------------- | ---------------------------------------------------------- |
| Model Version              | V2                                                         |
| Resource                   | NV GTX2080Ti; memory 11G; OS Ubuntu18.04                   |
| uploaded Date              | 07/23/2021                                                 |
| MindSpore Version          | 1.2.0                                                      |
| Training Parameters        | shufflenetv2_x1_0_imagenet2012.yaml                        |
| Optimizer                  | Momentum                                                   |
| Loss Function              | CrossEntropySmooth                                         |
| Backbone                   | ShuffleNetV2                                               |
| Neck                       | AvgPooling                                                 |
| Head                       | DenseHead                                                  |
| Batch size                 | 256                                                        |

## Eval setting

| Parameters                 | ShuffleNetV2                                               |
| -------------------------- | ---------------------------------------------------------- |
| Model Version              | V2                                                         |
| Resource                   | NV GTX2080Ti; memory 11G; OS Ubuntu18.04                   |
| uploaded Date              | 07/23/2021                                                 |
| MindSpore Version          | 1.2.0                                                      |
| Training Parameters        | shufflenetv2_x1_0_imagenet2012.yaml                        |
| Backbone                   | ShuffleNetV2                                               |
| Neck                       | AvgPooling                                                 |
| Head                       | DenseHead                                                  |
| Batch size                 | 256                                                        |

## Pretrained models

### ShuffleNetV2 ImageNet checkpoints

|         Model         | Params(M) | Flops(G) | Top-1 (%) | Top-5 (%) | Config | Download |
|:---------------------:|:---------:|:--------:|:---------:|:---------:|:---------:|:--------:|
| ShuffleNet V2_x0.5          |        |     |  |  | [config]() | [model]() |
| ShuffleNet V2_x1.0           |        |     | 69.65 | 88.63 | [config](https://gitee.com/mindspore/mindvision/blob/classification_shufflenetv2/mindvision/classification/config/shufflenetv2/shufflenetv2_x1_0_imagenet2012.yaml) | [model]() |
| ShuffleNet V2_x1.5           |        |     |  |  | [config]() | [model]() |
| ShuffleNet V2_x2.0           |        |     |  |  | [config]() | [model]() |

## Example

see this [link]() to learn more detail.
